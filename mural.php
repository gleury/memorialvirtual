<?php
require_once './class/Conexao.php';
require_once './class/mMemorial.php';
require_once './class/mFile.php';

$mMemorial = new mMemorial();
$mFile = new mFile('./assets/img/fotos');

$pagina = $_GET["pagina"] ?? 1;
// Maximo de registros por pagina
$maximo = 4;

// Calculando o registro inicial
$inicio = $pagina - 1;
$inicio = $maximo * $inicio;

$total = $mMemorial->CountRegistros();

$memorias = $mMemorial->listarComPaginacao($inicio, $maximo);


if (isset($_POST['cNovo'])) {

    $nome = $_POST['nome'];
    $snome = $_POST['snome'];
    $dn = $_POST['dn'];
    $df = $_POST['df'];
    $biografia = $_POST['biografia'];
    $foto = $_FILES['foto'];

    if ($mFile->ExisteFileForm($foto)) {
        if ($mFile->VerificarExtensaoPermitida($foto) === false) {
            header("Location: ./mural.php?anx=1");
            exit;
        }
        if ($mFile->VerificarTamanhoPermitido($foto) === false) {
            header("Location: ./mural.php?anx=2");
            exit;
        }

        if ($mFile->Upload($foto)) {
            if ($mMemorial->inserir($nome, $snome, $dn, $df, $biografia, $mFile->GetNameFile())) {
                header('Location: ./mural.php?ins=1');
                exit;
            } else {
                header('Location: ./mural.php?ins=0');
                exit;
            }
        }
    } else {
        if ($mMemorial->inserir($nome, $snome, $dn, $df, $biografia, 'no-photo.png')) {
            header('Location: ./mural.php?ins=1');
            exit;
        } else {
            header('Location: ./mural.php?ins=0');
            exit;
        }
    }
}
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Memorial Online Admin</title>

        <!-- Bootstrap core CSS -->
        <link href="./assets/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>

        <!-- Custom styles for this template -->
        <link href="./assets/custom.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
        <link href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>

        <header class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-body border-bottom shadow-sm">
            <p class="h5 my-0 me-md-auto fw-normal">Company name</p>
            <nav class="my-2 my-md-0 me-md-3">
                <a class="p-2 text-dark" href="#">Features</a>
                <a class="p-2 text-dark" href="#">Enterprise</a>
                <a class="p-2 text-dark" href="#">Support</a>
                <a class="p-2 text-dark" href="#">Pricing</a>
            </nav>
            <a class="btn btn-outline-primary" href="#">Sign up</a>
        </header>

        <main class="container">
            <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
                <h1 class="display-4">Memorias Virtuais</h1>
            </div>

            <div class="col-md-12">
                <?php if (isset($_GET['anx']) and $_GET['anx'] == 1) { ?>

                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        Extensão não permitida.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php } else if (isset($_GET['anx']) and $_GET['anx'] == 2) { ?>
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        Excedeu o tamanho da foto permitida.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['ins']) and $_GET['ins'] == 1) { ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        REGISTRO CADASTRADO COM SUCESSO.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php } else if (isset($_GET['ins']) and $_GET['ins'] == 0) { ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        ERRO AO CADASTRAR.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php } ?>
            </div>


            <div class="row mb-5">

                <form method="POST" enctype="multipart/form-data">
                    <div class="card" style="width: 22rem; border-radius: 20px;">
                        <div class="card-header text-center">
                            <h5> Nova Memória</h5>
                        </div>

                        <center><img id="view_foto" src="./assets/img/fotos/no-photo.png" class="card-img-top" style="max-width: 17.5rem; height: 17rem;"></center>
                        <div class="card-body">

                            <div class="mb-3">
                                <input class="form-control" accept="image/png, image/jpeg" name="foto" type="file" onchange="readURL('');" id="foto" required>
                            </div>

                            <div class="mb-3">

                                <div class="row">
                                    <div class="col-6">
                                        <label for="nome" class="form-label">Nome:</label>
                                        <input type="text" class="form-control" id="nome" name="nome">
                                    </div>
                                    <div class="col-6">
                                        <label for="snome" class="form-label">Sobrenome:</label>
                                        <input type="text" class="form-control" id="snome" name="snome">
                                    </div>
                                </div>

                            </div>

                            <div class="mb-3">

                                <div class="row">
                                    <div class="col-6">
                                        <label for="dn" class="form-label">Data de Nascimento:</label>
                                        <input type="date" class="form-control" id="dn" name="dn">
                                    </div>
                                    <div class="col-6">
                                        <label for="df" class="form-label">Data de Falecimento:</label>
                                        <input type="date" class="form-control" id="df" name="df">
                                    </div>
                                </div>

                            </div>


                            <div class="mb-3">
                                <label for="biografia" class="form-label">Mini Biografia:</label>
                                <textarea class="form-control" id="biografia" name="biografia" rows="3"></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary rounded-pill" style="float: right;" name="cNovo">Salvar</button>
                        </div>
                    </div>
                </form>

            </div>

            <div class="row text-center pt-5" id="conteudo">
                <?php foreach ($memorias as $m) { ?>
                    <div class="col-lg-3 mb-5">
                        <div class="card" style="width: 14.5rem; height: 550px; border-radius: 20px; padding: 0">
                            <img id="view_foto" src="./assets/img/fotos/<?= (strlen($m['foto']) > 0) ? $m['foto'] : 'no-photo.png'; ?>" class="card-img-top" style="width: auto; border-radius: 20px; height: 17rem;">
                            <div class="card-body">

                                <div class="mb-3">

                                    <h3 style="color: #ffc082"><?= ucfirst($m['nome']) . ' ' . ucfirst($m['sobrenome']) ?></h3>

                                </div>

                                <div class="mb-3">

                                    <h4><i class="far fa-star" style="font-size: 20px;"></i> <?= date_format(date_create($m['dn']), "Y") . ' - <i class="fas fa-cross" style="font-size: 20px;"></i> ' . date_format(date_create($m['df']), "Y") ?></h4>

                                </div>

                                <div class="mb-3" style="max-height: 85px; overflow-y: auto;">
                                    <center><?= $m['biografia'] ?></center>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <?php
                        $menos = $pagina - 1;
                        $mais = $pagina + 1;
                        $pgs = ceil($total / $maximo);
                        if ($pgs > 1) {
                            echo "<br />";
                            // Mostragem de pagina
                            if ($menos > 0) {
                                echo "<li class=\"page-item\"><a class=\"page-link\" href=\"" . $_SERVER['PHP_SELF'] . "?pagina={$menos}#conteudo\">Anterior</a></li>";
                            }
                            // Listando as paginas
                            for ($i = 1; $i <= $pgs; $i++) {
                                if ($i != $pagina) {
                                    echo "<li class=\"page-item\"><a class=\"page-link\" href=\"" . $_SERVER['PHP_SELF'] . "?pagina=" . ($i) . "#conteudo\">$i</a></li>";
                                } else {
                                    echo "<li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">$i</a></li>";
                                }
                            }
                            if ($mais <= $pgs) {
                                echo "<li class=\"page-item\"><a class=\"page-link\" href=\"" . $_SERVER['PHP_SELF'] . "?pagina={$mais}#conteudo\">Próxima</a></li>";
                            }
                        }
                        ?>
                    </ul>
                </nav>

            </div>

            <footer class="pt-4 my-md-5 pt-md-5 border-top">
                <div class="row">
                    <div class="col-12 col-md">
                        <img class="mb-2" src="./assets/brand/bootstrap-logo.svg" alt="" width="24" height="19">
                        <small class="d-block mb-3 text-muted">&copy; 2017–2021</small>
                    </div>
                </div>
            </footer>
        </main>

        <script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
        <script src="./assets/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
        <script type="text/javascript">
                                    $(document).ready(function () {
                                        $('#tabela').DataTable({
                                            "language": {
                                                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese-Brasil.json"
                                            }
                                        });
                                    });

                                    function readURL(id) {
                                        if (document.getElementById('foto' + id).files && document.getElementById('foto' + id).files[0]) {
                                            var reader = new FileReader();
                                            reader.onload = function (e) {
                                                $('#view_foto' + id)
                                                        .attr('src', e.target.result)
                                                        .width(300)
                                                        .height(300);
                                            };
                                            reader.readAsDataURL(document.getElementById('foto' + id).files[0]);
                                        }
                                    }

        </script>

    </body>
</html>
