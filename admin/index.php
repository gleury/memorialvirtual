<?php
require_once '../class/Conexao.php';
require_once '../class/mMemorial.php';
require_once '../class/mFile.php';

$mMemorial = new mMemorial();
$mFile = new mFile('../assets/img/fotos');

$memorias = $mMemorial->listar();


if (isset($_POST['cNovo'])) {

    $nome = $_POST['nome'];
    $snome = $_POST['snome'];
    $dn = $_POST['dn'];
    $df = $_POST['df'];
    $biografia = $_POST['biografia'];
    $foto = $_FILES['foto'];

    if ($mFile->ExisteFileForm($foto)) {
        if ($mFile->VerificarExtensaoPermitida($foto) === false) {
            header("Location: ./index.php?anx=1");
            exit;
        }
        if ($mFile->VerificarTamanhoPermitido($foto) === false) {
            header("Location: ./index.php?anx=2");
            exit;
        }

        if ($mFile->Upload($foto)) {
            if ($mMemorial->inserir($nome, $snome, $dn, $df, $biografia, $mFile->GetNameFile())) {
                header('Location: ./index.php?ins=1');
                exit;
            } else {
                header('Location: ./index.php?ins=0');
                exit;
            }
        }
    } else {
        if ($mMemorial->inserir($nome, $snome, $dn, $df, $biografia, 'no-photo.png')) {
            header('Location: ./index.php?ins=1');
            exit;
        } else {
            header('Location: ./index.php?ins=0');
            exit;
        }
    }
}

if (isset($_POST['cEditar'])) {

    $id_registro = $_POST['id_registro'];
    $nome = $_POST['nome'];
    $snome = $_POST['snome'];
    $dn = $_POST['dn'];
    $df = $_POST['df'];
    $biografia = $_POST['biografia'];
    $foto = $_FILES['foto'];

    if ($mFile->ExisteFileForm($foto)) {
        if ($mFile->VerificarExtensaoPermitida($foto) === false) {
            header("Location: ./index.php?anx=1");
            exit;
        }
        if ($mFile->VerificarTamanhoPermitido($foto) === false) {
            header("Location: ./index.php?anx=2");
            exit;
        }

        if ($mFile->Upload($foto)) {
            $fotoantiga = $mMemorial->GetFoto($id_registro);
            $mFile->DeleteFile($fotoantiga);
            if ($mMemorial->atualizarComFoto($nome, $snome, $dn, $df, $biografia, $mFile->GetNameFile(), $id_registro)) {
                header('Location: ./index.php?up=1');
                exit;
            } else {
                header('Location: ./index.php?up=0');
                exit;
            }
        }
    } else {
        if ($mMemorial->atualizar($nome, $snome, $dn, $df, $biografia, $id_registro)) {
            header('Location: ./index.php?up=1');
            exit;
        } else {
            header('Location: ./index.php?up=0');
            exit;
        }
    }
}

if (isset($_POST['cExcluir'])) {

    $id_registro = $_POST['id_registro'];
    $mFile->DeleteFile($mMemorial->GetFoto($id_registro));
    if ($mMemorial->excluir($id_registro)) {
        header('Location: ./index.php?ex=1');
    } else {
        header('Location: ./index.php?ex=0');
    }
}
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Memorial Online Admin</title>

        <!-- Bootstrap core CSS -->
        <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>

        <!-- Custom styles for this template -->
        <link href="../assets/custom.css" rel="stylesheet" type="text/css"/>
        <link href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>

        <header class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-body border-bottom shadow-sm">
            <p class="h5 my-0 me-md-auto fw-normal">Company name</p>
            <nav class="my-2 my-md-0 me-md-3">
                <a class="p-2 text-dark" href="#">Features</a>
                <a class="p-2 text-dark" href="#">Enterprise</a>
                <a class="p-2 text-dark" href="#">Support</a>
                <a class="p-2 text-dark" href="#">Pricing</a>
            </nav>
            <a class="btn btn-outline-primary" href="#">Sign up</a>
        </header>

        <main class="container">
            <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
                <h1 class="display-4">Memorias Virtuais</h1>
            </div>

            <div class="col-md-12">
                <?php if (isset($_GET['anx']) and $_GET['anx'] == 1) { ?>

                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        Extensão não permitida.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php } else if (isset($_GET['anx']) and $_GET['anx'] == 2) { ?>
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        Excedeu o tamanho da foto permitida.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['ins']) and $_GET['ins'] == 1) { ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        REGISTRO CADASTRADO COM SUCESSO.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php } else if (isset($_GET['ins']) and $_GET['ins'] == 0) { ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        ERRO AO CADASTRAR.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['up']) and $_GET['up'] == 1) { ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        REGISTRO ALTERADO COM SUCESSO.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php } else if (isset($_GET['up']) and $_GET['up'] == 0) { ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        ERRO AO ALTERAR.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['ex']) and $_GET['ex'] == 1) { ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        REGISTRO EXCLUÍDO COM SUCESSO.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php } else if (isset($_GET['ex']) and $_GET['ex'] == 0) { ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        ERRO AO EXCLUIR.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php } ?>
            </div>


            <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#Novo">
                Novo
            </button>

            <div class="row text-center">
                <table id="tabela">
                    <thead>
                        <tr>
                            <th>Foto</th>
                            <th>Nome</th>
                            <th>Sobrenome</th>
                            <th>Data de Nascimento</th>
                            <th>Data de Falecimento</th>
                            <th>Mini Biografia</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($memorias as $m) { ?>

                            <tr>
                                <td><img src="../assets/img/fotos/<?= (strlen($m['foto']) > 0) ? $m['foto'] : 'no-photo.png'; ?>" style="width: 200px; height: 200px;" /></td>
                                <td><?= $m['nome'] ?></td>
                                <td><?= $m['sobrenome'] ?></td>
                                <td><?= date_format(date_create($m['dn']), "d/m/Y") ?></td>
                                <td><?= date_format(date_create($m['df']), "d/m/Y") ?></td>
                                <td><textarea class="form-control" readonly="" style="resize: none;" rows="5"><?= $m['biografia'] ?></textarea></td>

                                <td>
                                    <div style="display:table;">
                                        <div style="display:table-row;">
                                            <div style="display:table-cell; padding-right: 3px;">
                                                <button type="button" class="btn btn-info mb-3" data-bs-toggle="modal" data-bs-target="#Editar<?= $m['id'] ?>">Editar</button>
                                            </div>

                                            <div style="display:table-cell; padding-right: 3px;">
                                                <button type="button" class="btn btn-danger mb-3" data-bs-toggle="modal" data-bs-target="#Excluir<?= $m['id'] ?>">Excluir</button>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        <?php } ?>
                    </tbody>
                </table>

            </div>

            <footer class="pt-4 my-md-5 pt-md-5 border-top">
                <div class="row">
                    <div class="col-12 col-md">
                        <img class="mb-2" src="../assets/brand/bootstrap-logo.svg" alt="" width="24" height="19">
                        <small class="d-block mb-3 text-muted">&copy; 2017–2021</small>
                    </div>
                    
                </div>
            </footer>
        </main>

        <script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
        <script src="../assets/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#tabela').DataTable({
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese-Brasil.json"
                    }
                });
            });

            function readURL(id) {
                if (document.getElementById('foto' + id).files && document.getElementById('foto' + id).files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#view_foto' + id)
                                .attr('src', e.target.result)
                                .width(300)
                                .height(300);
                    };
                    reader.readAsDataURL(document.getElementById('foto' + id).files[0]);
                }
            }

        </script>

        <!-- Modal -->
        <div class="modal fade" id="Novo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" enctype="multipart/form-data">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nova Memória</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <div class="mb-3">
                                <label for="foto" class="form-label">Foto</label>
                                <center><img id="view_foto" src="../assets/img/fotos/no-photo.png" style="margin:10px;width:200px;" class="rounded float-left"></center>
                                <input class="form-control" accept="image/png, image/jpeg" name="foto" type="file" onchange="readURL('');" id="foto" required>
                            </div>

                            <div class="mb-3">
                                <label for="nome" class="form-label">Nome:</label>
                                <input type="text" class="form-control" id="nome" name="nome">
                            </div>

                            <div class="mb-3">
                                <label for="snome" class="form-label">Sobrenome:</label>
                                <input type="text" class="form-control" id="snome" name="snome">
                            </div>

                            <div class="mb-3">
                                <label for="dn" class="form-label">Data de Nascimento:</label>
                                <input type="date" class="form-control" id="dn" name="dn">
                            </div>

                            <div class="mb-3">
                                <label for="df" class="form-label">Data de Falecimento:</label>
                                <input type="date" class="form-control" id="df" name="df">
                            </div>

                            <div class="mb-3">
                                <label for="biografia" class="form-label">Mini Biografia:</label>
                                <textarea class="form-control" id="biografia" name="biografia" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary" name="cNovo">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php foreach ($memorias as $m) { ?>

            <!-- Modal Editar -->
            <div class="modal fade" id="Editar<?= $m['id'] ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="POST" enctype="multipart/form-data">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Editar Memória</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>

                            <div class="modal-body">
                                <div class="mb-3">
                                    <label for="foto<?= $m['id'] ?>" class="form-label">Foto</label>
                                    <center><img id="view_foto<?= $m['id'] ?>" src="../assets/img/fotos/<?= (strlen($m['foto']) > 0) ? $m['foto'] : 'no-photo.png'; ?>" style="margin:10px;width:200px;" class="rounded float-left"></center>
                                    <input class="form-control" accept="image/png, image/jpeg" name="foto" type="file" onchange="readURL(<?= $m['id'] ?>);" id="foto<?= $m['id'] ?>">
                                </div>

                                <div class="mb-3">
                                    <label for="nome" class="form-label">Nome:</label>
                                    <input type="text" class="form-control" id="nome" name="nome" value="<?= $m['nome'] ?>">
                                </div>

                                <div class="mb-3">
                                    <label for="snome" class="form-label">Sobrenome:</label>
                                    <input type="text" class="form-control" id="snome" name="snome" value="<?= $m['sobrenome'] ?>">
                                </div>

                                <div class="mb-3">
                                    <label for="dn" class="form-label">Data de Nascimento:</label>
                                    <input type="date" class="form-control" id="dn" name="dn" value="<?= $m['dn'] ?>">
                                </div>

                                <div class="mb-3">
                                    <label for="df" class="form-label">Data de Falecimento:</label>
                                    <input type="date" class="form-control" id="df" name="df" value="<?= $m['df'] ?>">
                                </div>

                                <div class="mb-3">
                                    <label for="biografia" class="form-label">Mini Biografia:</label>
                                    <textarea class="form-control" id="biografia" name="biografia" rows="3"><?= $m['biografia'] ?></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="id_registro" value="<?= $m['id'] ?>">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                                <button type="submit" class="btn btn-primary" name="cEditar">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Excluir -->
            <div class="modal fade" id="Excluir<?= $m['id'] ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="POST" enctype="multipart/form-data">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Excluir Memória</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>

                            <div class="modal-body">
                                <h4>Deseja realmente excluir esse registro?</h4>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="id_registro" value="<?= $m['id'] ?>">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Não</button>
                                <button type="submit" class="btn btn-primary" name="cExcluir">Sim</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php } ?>
    </body>
</html>
