-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 15-Mar-2021 às 02:50
-- Versão do servidor: 10.4.14-MariaDB
-- versão do PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `memorial`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `memorial_virtual`
--

CREATE TABLE `memorial_virtual` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `sobrenome` varchar(255) NOT NULL,
  `dn` date NOT NULL,
  `df` date NOT NULL,
  `biografia` text NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `memorial_virtual`
--

INSERT INTO `memorial_virtual` (`id`, `nome`, `sobrenome`, `dn`, `df`, `biografia`, `foto`) VALUES
(11, 'Maria de', 'Fátima', '1943-05-12', '2021-02-02', 'Um momento que será lembrado para sempre, a primeira vez que viu sua netinha. Sempre muito carinhosa e amável com todos ao seu redor. Amamos você e sentimos muito a sua falta!', 'c802bb099d1e29cd9b377282446b1601.jpeg');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `memorial_virtual`
--
ALTER TABLE `memorial_virtual`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `memorial_virtual`
--
ALTER TABLE `memorial_virtual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
