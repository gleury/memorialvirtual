<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Memorial Virtual</title>

        <!-- Bootstrap core CSS -->

        <link href="./assets/dist/css/bootstrap.min.css" rel="stylesheet">

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>

        <!-- Custom styles for this template -->
        <link href="assets/custom.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>

        <header class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-body border-bottom shadow-sm">
            <p class="h5 my-0 me-md-auto fw-normal">Company name</p>
            <nav class="my-2 my-md-0 me-md-3">
                <a class="p-2 text-dark" href="#">Features</a>
                <a class="p-2 text-dark" href="#">Enterprise</a>
                <a class="p-2 text-dark" href="#">Support</a>
                <a class="p-2 text-dark" href="#">Pricing</a>
            </nav>
            <a class="btn btn-outline-primary" href="#">Sign up</a>
        </header>

        <main class="container">
            <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
                <h1 class="display-4">Memorial Virtual</h1>
                <p class="lead">Seja bem-vindo ao Memorial Virtual.</p>
                <p class="lead">Aqui você consegue eternizar lembranças de pessoas especiais que não estão mais entre nós, mas que para sempre serão lembradas.
                    Que tal homenagiá-las criando sua memória virtual de forma simples e rápida? Sempre que bater aquela saudade é só vir aqui e lembrar de momentos especiais!</p>
                
                <a href="./mural.php" class="btn btn-primary">Deixe sua memória</a>
            </div>

            <footer class="pt-4 my-md-5 pt-md-5 border-top">
                <div class="row">
                    <div class="col-12 col-md">
                        <img class="mb-2" src="./assets/brand/bootstrap-logo.svg" alt="" width="24" height="19">
                        <small class="d-block mb-3 text-muted">&copy; 2017–2021</small>
                    </div>
                    
                </div>
            </footer>
        </main>



    </body>
</html>
