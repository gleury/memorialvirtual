<?php

class Con {

    protected function Connect() {

        $host = "localhost";
        $user = "root";
        $bd = "memorial";
        $pass = "";

        try {

            $con = new \PDO("mysql:host={$host};port=3306;dbname={$bd}", "{$user}", "{$pass}");
            $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $con;
        } catch (Exception $exc) {
            return false;
        }
    }

}

?>
