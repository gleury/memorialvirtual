<?php

class mMemorial extends Con {

    public function inserir($nome, $snome, $dn, $df, $biografia, $foto) {

        $sql = "INSERT INTO memorial_virtual (nome, sobrenome, dn, df, biografia, foto) VALUES (:NOME, :SNOME, :DN, :DF, :BIOGRAFIA, :FOTO)";
        $con = $this->Connect();
        $stmt = $con->prepare($sql);
        $stmt->bindParam(":NOME", addslashes($nome), PDO::PARAM_STR);
        $stmt->bindParam(":SNOME", addslashes($snome), PDO::PARAM_STR);
        $stmt->bindParam(":DN", $dn, PDO::PARAM_STR);
        $stmt->bindParam(":DF", $df, PDO::PARAM_STR);
        $stmt->bindParam(":BIOGRAFIA", addslashes($biografia), PDO::PARAM_STR);
        $stmt->bindParam(":FOTO", $foto, PDO::PARAM_STR);

        try {
            return $stmt->execute();
        } catch (Exception $ex) {
            return false;
        }
    }

    public function atualizar($nome, $snome, $dn, $df, $biografia, $id) {
        $sql = "UPDATE memorial_virtual SET nome = :NOME, sobrenome = :SNOME, dn = :DN, df = :DF, biografia = :BIOGRAFIA WHERE id = :ID";
        $con = $this->Connect();
        $stmt = $con->prepare($sql);
        $stmt->bindParam(":NOME", addslashes($nome), PDO::PARAM_STR);
        $stmt->bindParam(":SNOME", addslashes($snome), PDO::PARAM_STR);
        $stmt->bindParam(":DN", $dn, PDO::PARAM_STR);
        $stmt->bindParam(":DF", $df, PDO::PARAM_STR);
        $stmt->bindParam(":BIOGRAFIA", addslashes($biografia), PDO::PARAM_STR);
        $stmt->bindParam(":ID", $id, PDO::PARAM_INT);

        try {
            return $stmt->execute();
        } catch (Exception $ex) {
            return false;
        }
    }

    public function atualizarComFoto($nome, $snome, $dn, $df, $biografia, $foto, $id) {
        $sql = "UPDATE memorial_virtual SET nome = :NOME, sobrenome = :SNOME, dn = :DN, df = :DF, biografia = :BIOGRAFIA, foto = :FOTO WHERE id = :ID";
        $con = $this->Connect();
        $stmt = $con->prepare($sql);
        $stmt->bindParam(":NOME", addslashes($nome), PDO::PARAM_STR);
        $stmt->bindParam(":SNOME", addslashes($snome), PDO::PARAM_STR);
        $stmt->bindParam(":DN", $dn, PDO::PARAM_STR);
        $stmt->bindParam(":DF", $df, PDO::PARAM_STR);
        $stmt->bindParam(":BIOGRAFIA", addslashes($biografia), PDO::PARAM_STR);
        $stmt->bindParam(":FOTO", $foto, PDO::PARAM_STR);
        $stmt->bindParam(":ID", $id, PDO::PARAM_INT);

        try {
            return $stmt->execute();
        } catch (Exception $ex) {
            return false;
        }
    }

    public function excluir($id) {
        $sql = "DELETE FROM memorial_virtual WHERE ID = :ID";
        $con = $this->Connect();
        $stmt = $con->prepare($sql);
        $stmt->bindParam(":ID", $id, PDO::PARAM_INT);
        try {
            return $stmt->execute();
        } catch (Exception $ex) {
            return false;
        }
    }

    public function listarComPaginacao($inicio, $maximo = 4) {
        $sql = "SELECT * FROM memorial_virtual LIMIT :INICIO, :MAXIMO";
        $con = $this->Connect();
        $stmt = $con->prepare($sql);
        $stmt->bindParam(":INICIO", $inicio, PDO::PARAM_INT);
        $stmt->bindParam(":MAXIMO", $maximo, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function listar() {
        $sql = "SELECT * FROM memorial_virtual";
        $con = $this->Connect();
        $stmt = $con->query($sql);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function CountRegistros() {
        $sql = "SELECT COUNT(1) AS CONT FROM memorial_virtual";
        $con = $this->Connect();
        $stmt = $con->query($sql);

        return $stmt->fetch(PDO::FETCH_ASSOC)['CONT'];
    }

    public function GetFoto($id) {
        $sql = "SELECT foto FROM memorial_virtual WHERE id = :ID";
        $con = $this->Connect();
        $stmt = $con->prepare($sql);
        $stmt->bindParam(":ID", $id, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC)['foto'];
    }

}
